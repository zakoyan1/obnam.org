#!/bin/bash
# From https://stackoverflow.com/questions/2458042/restore-a-files-modification-time-in-git

set -euo pipefail

rev=HEAD
for f in $(git ls-tree -r -t --full-name --name-only "$rev")
do
    echo "setting mtime for $f"
    touch -d "$(git log --pretty=format:%cI -1 "$rev" -- "$f")" "$f";
done
