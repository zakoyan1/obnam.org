[[!meta title="Contributing to Obnam"]]

The Obnam project welcomes contributions. If you want to help Obnam
become better, you can. There are many ways to contribute that don't
involve writing code.

* improve the documentation
  - fix typos or grammar
  - clarify some part
  - provide a helpful diagram
  - write missing parts of the documentation
* improve the website
  - fix typos or grammar
  - fix a layout problem
  - suggest a helpful link
  - make a stylish logo?
* try out the software and report your back your experiences
  - what went well? what could be improved?
* help others who have trouble with the software
  - on IRC, Matrix, or on the issue tracker
* help the developers understand what you need
  - document your use case
* fix a bug
* add a missing feature

We co-ordinate our work via the gitlab.com [issue tracker][]. Using it
requires an account on the site: if that's a problem for you, please
get in touch and we may be able to arrange something.

[issue tracker]: https://gitlab.com/larswirzenius/obnam/-/issues

# Getting Obnam to change so you can do what you want to do

Sometimes it happens you want Obnam to do something that it doesn't
quite know how to do. Here's some advice for that situation.

* For any substantial discussions, we prefer the issue tracker over
  chat systems. Chat systems are great for quick questions, but
  they're also ephemeral and only help the people who happen to be
  present at the time. The issue tracker lasts longer, and allows
  long-form replies and taking time to respond in depth.

* When suggesting or contributing a new feature, please always start
  by explaining the thing you want to achieve. "I want to back up a a
  historic computer's disk with a file system that isn't supported by
  Linux" is a better start than sending a patch for backing up raw
  disks. It's easier to judge a change fairly if the need for it is
  clear.

* If you contribute a functional change, please also change the
  automated test suite to verify the changed functionality changes. If
  you're not sure how to do that, please ask, and we'll help. We rely
  on our test suite to be able to make large changes rapidly and
  confidently. (Adding tests for bugs, when they're fixed, would be
  nice too, but we don't insist on that.)

Some caveats so you know what to expect:

* Obnam is a hobby project. It might take a while for us to respond.
  Please be patient. However, if you open an issue, and haven't heard
  back in a week, ping us on the issue or via a chat system. We try to
  be prompt, but sometimes work and life get in the way of working on
  hobby projects.


# Code review process

We use [gitlab.com](https://gitlab.com/larswirzenius/obnam) for code
hosting, and their merge request feature for reviewing changes.
However, most changes are currently made by Lars, the founder of the
project, and he is also the only one who can merge. Reviewing one's
own changes is less than ideal, as well as boring. Thus, to open the
door to code reviews by other people, the following process is in
place:

* Lars will push changes, and for each change set a value for N, which
  may be different for each change.
* Lars will wait for N days for comments, and if nothing has been
  raised that would prevent a merge, and if discussion isn't
  continuing, Lars will merge.
* Comments on merge requests on gitlab.com will be open: anyone will
  be able to comment.
* Lars may update the MR based on feedback, applying his best
  judgment.
  
Typical values for N will be:

* 0 for typo fixes and similar low-impact changes, or fixes to
  urgent high-impact issues (immediate self-merge)
* 1 for other urgent changes
* 3 for most changes &ndash; this will be the default
* 5 for changes likely to be controversial or affecting security
  related code

Lars will advertise the merge requests via various channels. The hope
is that this will eventually attract people to do reviews. As time
goes by, and trust is built, some of those people will get the
"approve" or "merge" privilege. The "approve" privilege allows marking
a merge request as "approved", allowing someone else to merge. The
"merge" one will allow telling GitLab to actually merge the change,
closing the merge request.

Merge requests by others will follow the same process. If nobody else
reviews a change, Lars will do that.

This process will be adjusted as needed as time goes by.
